<?php

// * Регулярний вираз для перевірки електронної адреси [RegExp]

$mails = array('test.mail@mail.com', 'abc-mail@host.ua', 'user@site.net', 'nomail@gmail.coma', '-mnk@mail.com',
	'mail@mail@mail.com', 'mail*tt@mail.com');


$pattern  = "/\A[a-z0-9]+([-._][a-z0-9]+)*@[a-zA-Z0-9.-]+\.(?:[a-zA-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum)$/";

for ($i = 0; $i < count($mails); $i++) { 
	print($mails[$i]);

	$r = preg_match($pattern , $mails[$i] );
	if ( !$r ) {
		echo( " - BAD! :-(");
	} else {
		echo " - OK :-)";
	}
	echo('</br>');
}

// * Реалізувати функцію транслітерації [PHP]

print(rus2translit("певна стрічка"));


function rus2translit($string) {
    $converter = array(
        'а' => 'a',   'б' => 'b',   'в' => 'v',
        'г' => 'g',   'д' => 'd',   'е' => 'e',
        'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
        'и' => 'i',   'й' => 'y',   'к' => 'k',
        'л' => 'l',   'м' => 'm',   'н' => 'n',
        'о' => 'o',   'п' => 'p',   'р' => 'r',
        'с' => 's',   'т' => 't',   'у' => 'u',
        'ф' => 'f',   'х' => 'h',   'ц' => 'c',
        'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
        'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
        'э' => 'e',   'ю' => 'yu',  'я' => 'ya',
        
        'А' => 'A',   'Б' => 'B',   'В' => 'V',
        'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
        'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
        'И' => 'I',   'Й' => 'Y',   'К' => 'K',
        'Л' => 'L',   'М' => 'M',   'Н' => 'N',
        'О' => 'O',   'П' => 'P',   'Р' => 'R',
        'С' => 'S',   'Т' => 'T',   'У' => 'U',
        'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
        'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
        'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
        'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
    );
    return strtr($string, $converter);
}

// * Вивести відсортований масив [PHP, arrays]

echo('</br>');

$a1 = array(7, 1, 4, 2, 5);
sort($a1);
echo implode( " ", $a1);

echo('</br>');

$a2 = array(
  'a1', 'a3', 'a22', 'a11'
);
natcasesort($a2);
array_unique($a2);
echo implode( " ", $a2);

// * Проходження і обробка масивів [PHP][arrays]

echo('</br>');

$a1 = array(‘el’, ‘ab’, ‘cd’);
$a2 = array(‘y5’, ‘y6’, ‘y7’);

print_r(Cat_arrs_with_separator($a1, $a2, "-"));

function Cat_arrs_with_separator($arr1, $arr2, $separator)
{
	$result = $arr1;
	if(count($arr1)!=count($arr2))
		return 0;
	else {
        $j=count($arr2);
		for ($i=0; $i < count($arr1); $i++) { 
			$j--;
            $result[$i] = $arr1[$i] . $separator . $arr2[$j];
		}
	}
	return $result;
}

// * Сортування масиву [PHP[arrays]

echo('</br>');

$unsorted = array(
  0 => 7,
  1 => 1,
  2 => 4,
  3 => 2,
  4 => 5
);


function quick_sort($array)
{
    $length = count($array);
    if($length <= 1){
        return $array;
    }
    else{
        $pivot = $array[0];
        $left = $right = array();
        for($i = 1; $i < count($array); $i++)
        {
            if($array[$i] < $pivot){
                $left[] = $array[$i];
            }
            else{
                $right[] = $array[$i];
            }
        }
        return array_merge(quick_sort($left), array($pivot), quick_sort($right));
    }
}

$sorted = quick_sort($unsorted);
print_r($sorted);

// * Реалізувати редірект на php [PHP][web]

// header( 'Location: http://google.ru/search?q=redirect' );

// * Відображення IP-адреси клієнта [PHP][web]

echo('</br>');

echo( '<h2> Hello, Your IP is ' . $_SERVER['REMOTE_ADDR'] . '</h2>');

// * Реалізувати простий калькулятор [PHP, HTML][web]

echo "<a href=\"/magnetic_test/calculator.php\"> CALCULATOR </a> </br>";

// * Виведення вмісту CSV файла у вигляді масиву [PHP][CSV]

echo "<a href=\"/magnetic_test/readCSV.php\"> Read CSV </a> </br>";

// * Запис даних у  CSV файл [PHP][CSV]

echo "<a href=\"/magnetic_test/writeCSV.php\"> Write CSV </a> </br>";

// * Вибірка пов’язаних даних [MySQL]

// SELECT categories.c_name, products.p_name FROM categories, products, associations WHERE associations.c_id = categories.c_id && associations.p_id = products.p_id ;

// * Вибірка даних і записування їх в CSV [PHP, MySQL][CSV]

echo "<a href=\"/magnetic_test/mySQL_write_CSV.php\"> MySQL Write to CSV </a> </br>";

// * Видалення дублікатів [MySQL]

// CREATE TEMPORARY TABLE `table_temp` as ( SELECT min(id) as id FROM test_table GROUP BY key_);
// DELETE from test_table WHERE test_table.id not in ( SELECT id FROM table_temp );

?>