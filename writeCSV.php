<?php 

$a1 = array(
	"name" => 'some name',
	"age" => 5,
	"city" => 'some town'
);

$a2 = array(
	"age" => 6,
	"country" => 'small country',
	"city" => 'mego city',
	"street" => 'cute ave.'
);

function ConnectArrays($arr1, $arr2)
{
	$indexes = array();
	$fixed_indexes = array(); // fixes prev array ( [0] [1] [2] [3] [4] [6] ; - [5] out of range exeption )

	while (current($arr1)) {
		array_push($indexes, key($arr1));
		next($arr1);
	}
	while (current($arr2)) {
		array_push($indexes, key($arr2));
		next($arr2);
	}
	$indexes = array_unique($indexes);

	// Fix out of range (read coment for [$fixed_indexes] )
	while ($tmp_index = current($indexes)) {
		array_push($fixed_indexes, $tmp_index);
		next($indexes);
	}
	//------------------------------------
	$result = array(
		 0 => new SplFixedArray(count($fixed_indexes)), 
		 1 => new SplFixedArray(count($fixed_indexes)));
	//------------------------------------
	if(!current($arr1)) { reset($arr1); }
	while ($tmp = current($arr1)) {
		$indexs_str = key($arr1);
		$index = array_search($indexs_str, $fixed_indexes);
		$result[0][$index] = $tmp;
		next($arr1);
	}

	if(!current($arr2)) { reset($arr2); }
	while ($tmp = current($arr2)) {
		$indexs_str = key($arr2);
		$index = array_search($indexs_str, $fixed_indexes);
		$result[1][$index] = $tmp;
		next($arr2);
	}

	$final_result = array();
	array_push($final_result, $fixed_indexes);
	array_push($final_result, $result[0]);
	array_push($final_result, $result[1]);
	
	return $final_result;
}

//------------------

$file = fopen("files/file3.csv","w");

$table = ConnectArrays($a1, $a2);

foreach ($table as $line)
{
	fputcsv($file, (array)$line);
}

fclose($file);

echo 'Writting finished succesfully!!!';

?>
