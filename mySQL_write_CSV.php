<?php

$servername = "localhost";
$username = "**";
$password = "**";
$dbname = "magnetic_test";


$indexes = array( 0 => 'c_name', 1 => 'p_name');
$values = array();
array_push($values, implode(",", $indexes));


// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql = 'SELECT categories.c_name, products.p_name FROM categories, products, associations WHERE associations.c_id = categories.c_id && associations.p_id = products.p_id';
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        array_push($values, $row[$indexes[0]]. "," . $row[$indexes[1]]);
    }
} else {
    echo "0 results";
}

$file = fopen("files/file2.csv","w");

foreach ($values as $line)
{
	fputcsv($file, explode(",", $line));
}

fclose($file);

echo 'Writting finished succesfully!!!';

$conn->close();
?>